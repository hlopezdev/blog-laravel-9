<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema; //Siempre agregar para evitar errores
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191); //Siempre agregar para evitar errores
    }
}
