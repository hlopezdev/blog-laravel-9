<?php

use Illuminate\Support\Facades\Route;

use Illuminate\Http\Request;

use App\Http\Controllers\PageController;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get Consultar
// Route::post Crear
// Route::put Actualizar
// Route::delete Eliminar

//Ejemplo de ruta

// Route::get('/', [PageController::class, 'home'])->name('home');

// Route::get('/blog', [PageController::class, 'blog'])->name('blog');

// Route::get('/blog/{slug}', [PageController::class, 'post'])->name('post');

// Route::get('/buscar', function (Request $request) {
//     if ($request->has('q')) {
//         if ($request->q == 'laravel') {
//             return 'Resultado de la busqueda: ' . $request->q;
//         }else{
//             return 'No se encontro la palabra: ' . $request->q;
//         }
//     }else{
//         return 'No se ha ingresado ningun valor';
//     }
// });

Route::controller(PageController::class)->group(function () {
    Route::get('/', 'home')->name('home');
    // Route::get('/blog', 'blog')->name('blog');
    Route::get('/blog/{post:slug}', 'post')->name('post');
});

Route::redirect('dashboard', 'posts')->name('dashboard');

Route::resource('posts', PostController::class)->middleware('auth')->except(['show']);

require __DIR__.'/auth.php';
